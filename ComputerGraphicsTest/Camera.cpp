#include "Camera.h"
#include <glm/ext.hpp>

const float deg2Rad = 3.14159265f / 180.0f;

glm::mat4 Camera::GetProjectionMatrix(float w, float h)
{
	return glm::perspective(glm::pi<float>() * 0.25f, w / h, 0.1f, 1000.f);
}

glm::mat4 Camera::GetViewMatrix()
{
	float thetaR = theta * deg2Rad;
	float phiR = phi * deg2Rad;
	glm::vec3 forward(cos(phiR)*cos(thetaR), sin(phiR), cos(phiR)*sin(thetaR));
	return glm::lookAt(position, position + forward, glm::vec3(0, 1, 0));
}

void Camera::MoveCamera(directions direction, float deltaTime)
{
	float thetaR = theta * deg2Rad;
	float phiR = phi * deg2Rad;

	glm::vec3 forward(cos(phiR)*cos(thetaR), sin(phiR), cos(phiR)*sin(thetaR));
	glm::vec3 right(-sin(thetaR), 0, cos(thetaR));
	glm::vec3 up(0, 1, 0);

	switch (direction)
	{
	case UP:
		position += up * deltaTime;
		break;
	case DOWN:
		position += -up * deltaTime;
		break;
	case FORWARD:
		position += forward * deltaTime;
		break;
	case BACKWARD:
		position += -forward * deltaTime;
		break;
	case LEFT:
		position += -right * deltaTime;
		break;
	case RIGHT:
		position += right * deltaTime;
		break;
	}
}

void Camera::RotateCamera(float mouseX, float mouseY, bool move)
{
	if (move)
	{
		theta += 0.05f * (mouseX - lastX);
		phi += 0.05f * (mouseY - lastY);
	}
	lastX = mouseX;
	lastY = mouseY;
}
