#pragma once
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

enum directions
{
	UP = 0,
	DOWN,
	LEFT,
	RIGHT,
	FORWARD,
	BACKWARD
};

class Camera
{
public:
	Camera() : theta(0), phi(-20), position(-10, 4, 0) {}

	glm::mat4 GetProjectionMatrix(float w, float h);
	glm::mat4 GetViewMatrix();

	void MoveCamera(directions direction, float deltaTime);
	void RotateCamera(float mouseX, float mouseY, bool move);

	glm::vec3 GetPos() { return position; }

private:
	float theta;
	float phi;
	glm::vec3 position;
	float lastX;
	float lastY;
};

