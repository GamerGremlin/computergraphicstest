#pragma once
#include <glm/vec4.hpp>

using glm::vec4;


namespace colors
{
	const vec4 white = vec4(1);
	const vec4 black = vec4(0, 0, 0, 1);
}