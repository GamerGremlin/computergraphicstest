// ComputerGraphicsTest.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "gl_core_4_4.h"
#include "Gizmos.h"
#include "Program.h"

using glm::vec3;
using glm::mat4;
using glm::vec4;

using namespace aie;

int main()
{
	bool runProgram = true;
	Program* mainProgram = nullptr;
	try
	{
		mainProgram = new Program(1920, 1080, "Computer Graphics Test");
	}
	catch (std::exception& e)
	{
		std::cout << "The following error has occured: " << e.what() << std::endl;
		runProgram = false;
		system("pause");
	}


	//Time variables
	double previousTime = glfwGetTime();
	double currentTime = 0;
	double deltaTime = 0;
	double fpsInterval = 0;

	if (!runProgram)
	{
		return -1;
	}

	while (runProgram)
	{
		currentTime = glfwGetTime();
		deltaTime = currentTime - previousTime;
		previousTime = currentTime;

		if(!(mainProgram->Update((float)deltaTime)))
			runProgram = false;
		mainProgram->Draw(true);
	}

	return 0;
}
