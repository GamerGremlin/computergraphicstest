#pragma once
#include <exception>

namespace glfwExcept
{
	class InitFailure : public std::exception
	{
		virtual const char * what() const throw()
		{
			return "GLFW Initilization failed.";
		}
	}initFail;

	class WindowFailure : public std::exception
	{
		virtual const char * what() const throw()
		{
			return "GLFW Window Creation failed.";
		}
	}windowFail;

	class FunctionFailure : public std::exception
	{
		virtual const char * what() const throw()
		{
			return "GLFW Function Pointer Load failed.";
		}
	}funcFail;
}