#include "Program.h"
#include "Colors.h"
#include "Gizmos.h"
#include "Input.h"
#include "GLFW Exceptions.h"

Program::Program(unsigned int windowWidth, unsigned int windowHeight, const char * windowName)
{
	if (glfwInit() == false)
	{
		throw glfwExcept::initFail;
		return;
	}

	window = glfwCreateWindow(windowWidth, windowHeight, windowName, nullptr, nullptr);

	if (window == nullptr)
	{
		glfwTerminate();
		throw glfwExcept::windowFail;
	}

	glfwMakeContextCurrent(window);

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		glfwDestroyWindow(window);
		glfwTerminate();
		throw glfwExcept::funcFail;
	}

	glClearColor(0.25, 0.25, 0.25, 1);
	glEnable(GL_DEPTH_TEST);

	glfwMajorVersion = ogl_GetMajorVersion();
	glfwMinorVersion = ogl_GetMinorVersion();

	firstTexture = new aie::Texture();

	aie::Gizmos::create(25565, 65565, 65565, 65565);

	aie::Input::create();

	cam = new Camera();

	modelMesh = new aie::OBJMesh();

	shader = new aie::ShaderProgram();

	shader->loadShader(aie::eShaderStage::VERTEX, "./data/shaders/normalMapVert.glsl");
	shader->loadShader(aie::eShaderStage::FRAGMENT, "./data/shaders/normalMapFrag.glsl");
	if (shader->link() == false)
	{
		printf("Shader Error: %s\n", shader->getLastError());
		return;
	}

	if (firstTexture->load("./data/textures/numbered_grid.tga") == false)
	{
		printf("Failed to load texture.\n");
		return;
	}

	if (modelMesh->load("./data/models/souls/soulspear.obj", true, true) == false)
	{
		printf("Mesh Error!\n");
		return;
	}
	
	modelTransform = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};

	light.diffuse = { 1, 1, 0 };
	light.specular = { 1, 1, 0 };
	ambientLight = { 0.25f, 0.25f, 0.25f };
}

Program::~Program()
{
	delete cam;
	delete modelMesh;
	delete shader;
	aie::Input::destroy();
	aie::Gizmos::destroy();
	glfwDestroyWindow(window);
	glfwTerminate();
}

const float increaseBy = 0.25f;

bool Program::Update(float deltaTime)
{
	aie::Input* input = aie::Input::getInstance();
	input->clearStatus();

	if (input->isKeyDown(aie::INPUT_KEY_W))
		cam->MoveCamera(directions::FORWARD, deltaTime);
	if (input->isKeyDown(aie::INPUT_KEY_S))
		cam->MoveCamera(directions::BACKWARD, deltaTime);
	if (input->isKeyDown(aie::INPUT_KEY_A))
		cam->MoveCamera(directions::LEFT, deltaTime);
	if (input->isKeyDown(aie::INPUT_KEY_D))
		cam->MoveCamera(directions::RIGHT, deltaTime);
	if (input->isKeyDown(aie::INPUT_KEY_Q))
		cam->MoveCamera(directions::UP, deltaTime);
	if (input->isKeyDown(aie::INPUT_KEY_E))
		cam->MoveCamera(directions::DOWN, deltaTime);

	if (input->isMouseButtonDown(aie::INPUT_MOUSE_BUTTON_RIGHT))
		cam->RotateCamera((float)input->getMouseX(), (float)input->getMouseY(), true);
	else
		cam->RotateCamera((float)input->getMouseX(), (float)input->getMouseY(), false);

	if (input->isKeyDown(aie::INPUT_KEY_O))
		lightValue += increaseBy;
	if (input->isKeyDown(aie::INPUT_KEY_L))
		lightValue -= increaseBy;

	light.direction = glm::normalize(glm::vec3(glm::cos(lightValue), glm::sin(lightValue), 0));

	//Ensure everything else happens before this
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		return false;
	else
		return true;
}

void Program::Draw(bool debugGrid)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	aie::Gizmos::clear();

	shader->bind();

	shader->bindUniform("Ia", ambientLight);
	shader->bindUniform("Id", light.diffuse);
	shader->bindUniform("Is", light.specular);
	shader->bindUniform("lightDirection", light.direction);
	shader->bindUniform("cameraPosition", cam->GetPos());

	auto pvm = cam->GetProjectionMatrix((float)GetWindowWidth(), (float)GetWindowHeight()) * cam->GetViewMatrix() * modelTransform;
	shader->bindUniform("ProjectionViewModel", pvm);

	shader->bindUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(modelTransform)));

	modelMesh->draw();

	if (debugGrid)
		DrawGrid();

	aie::Gizmos::draw(cam->GetProjectionMatrix((float)GetWindowWidth(), (float)GetWindowHeight()) * cam->GetViewMatrix());

	glfwSwapBuffers(window);
	glfwPollEvents();
}

unsigned int Program::GetWindowWidth()
{
	int w = 0, h = 0;
	glfwGetWindowSize(window, &w, &h);
	return w;
}

unsigned int Program::GetWindowHeight()
{
	int w = 0, h = 0;
	glfwGetWindowSize(window, &w, &h);
	return h;
}

void Program::DrawGrid()
{
	aie::Gizmos::addTransform(glm::mat4(1));

	for (int i = 0; i < 21; i++)
	{
		aie::Gizmos::addLine(glm::vec3(-10 + i, 0, 10), glm::vec3(-10 + i, 0, -10), i == 10 ? colors::white : colors::black);

		aie::Gizmos::addLine(glm::vec3(10, 0, -10 + i), glm::vec3(-10, 0, -10 + i), i == 10 ? colors::white : colors::black);
	}
}