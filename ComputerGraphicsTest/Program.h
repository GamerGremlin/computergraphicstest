#pragma once
#include "gl_core_4_4.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include "Camera.h"
#include "OBJMesh.h"
#include "Shader.h"
#include "Texture.h"

class Program
{
public:
	Program(unsigned int windowWidth, unsigned int windowHeight, const char* windowName);
	~Program();

	bool Update(float deltaTime);
	void Draw(bool debugGrid);

	unsigned int GetWindowWidth();
	unsigned int GetWindowHeight();


protected:

	void DrawGrid();

	GLFWwindow* window;
	Camera* cam;

	aie::OBJMesh* modelMesh;
	aie::ShaderProgram* shader;
	aie::Texture* firstTexture;

	struct Light {
		glm::vec3 direction;
		glm::vec3 diffuse;
		glm::vec3 specular;
	};

	float lightValue;

	Light light;
	glm::vec3 ambientLight;

	glm::mat4 modelTransform;

	int glfwMajorVersion;
	int glfwMinorVersion;
};

