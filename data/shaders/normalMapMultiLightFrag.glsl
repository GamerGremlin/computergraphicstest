#version 410

//This thing is broke

in vec2 vTexCoord;
in vec3 vNormal;
in vec3 vTangent;
in vec3 vBiTangent;
in vec4 vPosition;

out vec4 FragColour;

uniform sampler2D diffuseTexture;
uniform sampler2D specularTexture;
uniform sampler2D normalTexture;

//Material

uniform vec3 Ka; //ambient
uniform vec3 Kd; //diffuse
uniform vec3 Ks; //specular
uniform float specularPower;

uniform vec3 Ia; //Singular ambient light value

//Lights

const unsigned int maxLights = 8;

uniform unsigned int lightCount;

uniform vec3 lightDiffuses[maxLights]; //diffuse
uniform vec3 lightSpeculars[maxLights]; //specular
uniform vec3 lightDirections[maxLights]; //directions

uniform vec3 cameraPosition;

float getDiffuse(vec3 lightDir)
{
	return max(0, dot(vNormal.xyz, lightDir));
}


void main()
{
	vec3 N = normalize(vNormal);
	vec3 T = normalize(vTangent);
	vec3 B = normalize(vBiTangent);
	vec3 V = normalize(cameraPosition - vPosition.xyz);
	
	vec3 texDiffuse = texture( diffuseTexture, vTexCoord).rgb;
	vec3 texSpecular = texture( specularTexture, vTexCoord ).rgb;
	vec3 texNormal = texture( normalTexture, vTexCoord ).rgb;
	
	mat3 TBN = mat3(T,B,N);
	N = TBN * (texNormal * 2 - 1);
	
	vec3 diffuse = vec3(0, 0, 0);
	vec3 specular = vec3(0, 0, 0);
	
	for(int i = 0; i < lightCount; i++)
	{
		vec3 lightDir = normalize(lightDirections[i]);
		
		float lambertTerm = max(0, dot(N, -lightDir));
		
		vec3 R = reflect(lightDir, N);
		
		float specularTerm = pow(max(0, dot(R, V ) ), specularPower);
		
		diffuse += lightDiffuses[i] * Kd * texDiffuse * lambertTerm;
		specular += lightSpeculars[i] * Ks * texSpecular * specularTerm;
	}

vec3 ambient = Ia * Ka;

FragColour = vec4(ambient + specular + diffuse, 1);
}